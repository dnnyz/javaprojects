import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterRunnable implements Runnable {
    private final static String DELIMITER = " ";
    private final ConcurrentHashMap<String, AtomicInteger> counter;
    private String fileName;

    public CounterRunnable(String fileName, ConcurrentHashMap<String, AtomicInteger> counter) {
        this.counter = counter;
        this.fileName = fileName;
    }

    public void countWord(String word) {
        AtomicInteger count = counter.get(word);
        if (count == null) {
            count = new AtomicInteger(1);
            count = counter.putIfAbsent(word, count);
            if (count != null) {
                count.incrementAndGet();
            }
        } else {
            count.incrementAndGet();
        }
    }

    private File getFileFromResources(String fileName) throws IllegalArgumentException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("File: " + fileName + " is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    @Override
    public void run() {
        try {
            File file = getFileFromResources(fileName);
            BufferedReader buffer = new BufferedReader(new FileReader(file));

            String line;
            while ((line = buffer.readLine()) != null) {
                String[] strArr = line.split(DELIMITER);

                for (String word : strArr) {
                    countWord(word);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
