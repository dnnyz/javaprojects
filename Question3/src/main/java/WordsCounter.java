import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class WordsCounter {
    private final static int NUM_THREADS = 3;
    private final ConcurrentHashMap<String, AtomicInteger> counter;

    public WordsCounter() {
        counter = new ConcurrentHashMap<>();
    }

    public static void main(String[] args) {
        WordsCounter wc = new WordsCounter();
        // load text files in parallel
        wc.load("file1.txt", "file2.txt", "file3.txt");
        // display words statistics
        wc.displayStatus();
    }

    public void load(String... files) {
        ExecutorService executor = Executors.newFixedThreadPool(NUM_THREADS);
        try {
            for (String fileName : files) {
                executor.execute(new CounterRunnable(fileName, counter));
            }
            executor.shutdown();

            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            executor.shutdownNow();
        }
    }

    public void displayStatus() {
        int count = 0;
        for (Map.Entry<String, AtomicInteger> entry : counter.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
            count += entry.getValue().intValue();
        }

        System.out.println("** total: " + count);
    }
}
