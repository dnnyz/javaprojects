# Question 3
Words Counter
In this exercise, you are asked to write a Class (WordsCounter.java) that will receive few text
files and will count (and later print) all the words that exist in these files (together) and the
number of times each one of them appears.
The class should contain a single map that will hold the words and the number of items of each.
As can be seen in the example below, the method “load” can get few files. Each file should be
handled in separate threads (in parallel) .
The method “displayStatus” will print the map.
Specific guidance :
please use java.util.concurrent.ConcurrentHashMap and also it’s method putIfAbsent()
Note: When splitting the text into separate words, There is no need to deal with special
characters such as “,.-:” etc.

public class WordsCounter {

	public static void main (String [] args) {
		WordsCounter wc=new WordsCounter ();
		// load text files in parallel
		wc.load("file1.txt","file2.txt","file3.txt");
		// display words statistics
		wc.displayStatus();
	}
}

Small example (it is recommended to use files with many words):
File1.txt:
this is the first file
File2.txt:
this one is the second file
File3.txt:
and this is the third file
Output of displayStatus method:
and 1
file 3
first 1
is 3
one 1
second 1
the 3
third 1
this 3
** total: 17
You should send the working code as well as the input text files you used, and also the
displayStatus output as you got it when executed the code on these files.


## License
[MIT](https://choosealicense.com/licenses/mit/)