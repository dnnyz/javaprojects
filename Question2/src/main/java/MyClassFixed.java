import java.util.Date;
import java.util.List;

public class MyClassFixed {
    private Date time;
    private String name;
    private List<Long> numbers;
    private List<String> strings;

    public MyClassFixed(Date time, String name, List<Long> numbers, List<String> strings) {
        this.time = time;
        this.name = name;
        this.numbers = numbers;
        this.strings = strings;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        MyClassFixed copyClass = (MyClassFixed) obj;

        return name.equals(copyClass.name) &&
                time.compareTo(copyClass.time) == 0 &&
                numbers.size() == copyClass.numbers.size() &&
                strings.size() == copyClass.strings.size() &&
                numbers.equals(copyClass.numbers) &&
                strings.equals(copyClass.strings);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) time.getTime();
        hash = 31 * hash + (null == name ? 0 : name.hashCode());
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(name);
        if (numbers != null) {
            numbers.forEach(num -> builder.append(" ").append(num));
        }

        return builder.toString();
    }

    public void removeString(String str) {
        if (strings != null) {
            strings.remove(str);
        }
    }

    public boolean containsNumber(long number) {
        if (numbers != null) {
            return numbers.contains(number);
        }

        return false;
    }

    public boolean isHistoric() {
        return time.before(new Date(System.currentTimeMillis()));
    }
}