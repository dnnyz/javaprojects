import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InvoiceDigitTest {

    private InvoiceDigit invoiceDigit;

    @BeforeEach
    public void setUp() {
        invoiceDigit = new InvoiceDigit();

        Segment seg = new Segment(0, " _ ");
        invoiceDigit.parseSegment(0, seg);
        seg = new Segment(0, "|_ ");
        invoiceDigit.parseSegment(1, seg);
        seg = new Segment(0, "|_|");
        invoiceDigit.parseSegment(2, seg);
        seg = new Segment(0, "   ");
        invoiceDigit.parseSegment(3, seg);
    }

    @AfterEach
    public void tearDown() {
        invoiceDigit = null;
    }

    @Test
    public void getNumber() {
        int digit = invoiceDigit.getNumber();
        Assertions.assertEquals(6, digit);
    }

    @Test
    public void toStringTest() {
        String digit = invoiceDigit.toString();
        Assertions.assertEquals("6", digit);

        invoiceDigit = new InvoiceDigit();

        Segment seg = new Segment(0, " _ ");
        invoiceDigit.parseSegment(0, seg);
        seg = new Segment(0, " _ ");
        invoiceDigit.parseSegment(1, seg);
        seg = new Segment(0, " _ ");
        invoiceDigit.parseSegment(2, seg);
        seg = new Segment(0, "   ");
        invoiceDigit.parseSegment(3, seg);
        digit = invoiceDigit.toString();
        Assertions.assertEquals("?", digit);
    }
}