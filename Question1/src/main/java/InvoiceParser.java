import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class InvoiceParser {
    private List<InvoiceNumber> invoiceList;

    public InvoiceParser() {
        invoiceList = new ArrayList<>();
    }

    private void parseData(BufferedReader reader) throws IOException {
        InvoiceNumber number;
        while ((number = new InvoiceNumber()).readNumbers(reader)) {
            invoiceList.add(number);
        }
    }

    public void processFile(String fileName) {
        File file = null;
        try {
            file = getFileFromResources(fileName);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        if(file != null){

            try (BufferedReader buffer = new BufferedReader(new FileReader(file))) {
                parseData(buffer);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        else{
            throw new IllegalArgumentException("File: " + fileName + " is not found!");
        }
    }

    private File getFileFromResources(String fileName) throws IllegalArgumentException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("File: " + fileName + " is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    public void printToFile(String fileName) {
        File file = new File(fileName);
        try (PrintWriter writer = new PrintWriter(file)) {
            if (invoiceList != null) {
                invoiceList.forEach(invoiceNumber -> writer.println(invoiceNumber.toString()));
            }
        } catch (IOException e) {
            //Write to log
            System.out.println(e.getMessage());
        }
    }
}