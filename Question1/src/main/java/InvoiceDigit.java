enum SegmentLine {
    TOP, MIDDLE, BOTTOM,
}

public class InvoiceDigit {
    private byte binaryDigit;

    public InvoiceDigit() {
        binaryDigit = 0;
    }

    /**
     * @return Invoice digit between 0-9 after casting from binary representation
     */
    public int getNumber() {
        switch (binaryDigit) {
            case 6:
                return 1;

            case 91:
                return 2;

            case 79:
                return 3;

            case 102:
                return 4;

            case 109:
                return 5;

            case 125:
                return 6;

            case 7:
                return 7;

            case 127:
                return 8;

            case 111:
                return 9;

            case 63:
                return 0;

            default:
                return -1;
        }
    }

    private boolean isLegal() {
        return getNumber() >= 0;
    }

    /**
     * @return Valid Number as String between 0-9 or '?'
     */
    @Override
    public String toString() {
        return isLegal() ? String.valueOf(getNumber()) : "?";
    }

    /**
     * @param lineIndex current position of Line
     * @param segment 3 chars of the line
     * @return this
     */
    public InvoiceDigit parseSegment(int lineIndex, Segment segment) {
        switch (SegmentLine.values()[lineIndex]) {
            case TOP:
                if (segment.getCenter() == '_') {
                    binaryDigit |= 1;
                }
                break;

            case MIDDLE:
                if (segment.getCenter() == '_') {
                    binaryDigit |= 64;
                }

                if (segment.getLeft() == '|') {
                    binaryDigit |= 32;
                }

                if (segment.getRight() == '|') {
                    binaryDigit |= 2;
                }
                break;

            case BOTTOM:
                if (segment.getCenter() == '_') {
                    binaryDigit |= 8;
                }

                if (segment.getLeft() == '|') {
                    binaryDigit |= 16;
                }

                if (segment.getRight() == '|') {
                    binaryDigit |= 4;
                }
                break;

            default:
                break;
        }
        return this;
    }
}
