public class Segment {
    private final int SEGMENT_LENGTH = 3;

    private char left;
    private char center;
    private char right;
    private boolean isValid = false;

    public Segment(int beginIndex, String str) {
        int startIndex = beginIndex * SEGMENT_LENGTH;
        int endIndex = startIndex + SEGMENT_LENGTH;

        if (str.length() >= endIndex) {
            String segmentStr = str.substring(startIndex, endIndex);
            left = segmentStr.charAt(0);
            center = segmentStr.charAt(1);
            right = segmentStr.charAt(2);
            isValid = true;
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public char getLeft() {
        return left;
    }

    public char getCenter() {
        return center;
    }

    public char getRight() {
        return right;
    }

}
