public class Main {

    public static void main(String[] args) {
        InvoiceParser invoiceParser = new InvoiceParser();
        invoiceParser.processFile("input_Q1a.txt");
        invoiceParser.printToFile("output_Q1a.txt");

        InvoiceParser invoicesWithErr = new InvoiceParser();
        invoicesWithErr.processFile("input_Q1b.txt");
        invoicesWithErr.printToFile("output_Q1b.txt");
    }
}
