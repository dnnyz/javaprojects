import java.io.BufferedReader;
import java.io.IOException;

public class InvoiceNumber {
    private final int SEGMENT_LINE = 4;
    private final int SEGMENT_INDEX = 9;
    private InvoiceDigit[] digitsArr;

    public InvoiceNumber() {
        digitsArr = new InvoiceDigit[SEGMENT_INDEX];
        for (int i = 0; i < digitsArr.length; i++) {
            digitsArr[i] = new InvoiceDigit();
        }
    }

    /**
     * @return All parsed invoices numbers including the illegal digit with message 'ILLEGAL'
     */
    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        boolean isIllegal = false;
        for (InvoiceDigit digit : digitsArr) {
            if (digit.toString().contains("?")) {
                isIllegal = true;
            }
            output.append(digit.toString());
        }

        return isIllegal ? output.append(" ILLEGAL").toString() : output.toString();
    }

    /**
     * @param reader Stream of the file
     * @return True: Successfully processed all 4 lines and saved 9 invoice digits to array
     * False: We reach end of the stream
     * @throws IOException
     */
    public boolean readNumbers(BufferedReader reader) throws IOException {
        String line;
        for (int lineCounter = 0; lineCounter < SEGMENT_LINE; lineCounter++) {
            if ((line = reader.readLine()) != null) {
                for (int digitCounter = 0; digitCounter < SEGMENT_INDEX; digitCounter++) {
                    Segment segment = new Segment(digitCounter, line);
                    if (segment.isValid()) {
                        digitsArr[digitCounter].parseSegment(lineCounter, segment);
                    }
                }
            } else {
                return false; //End of file
            }
        }
        return true; //End of Segment Digit
    }
}
